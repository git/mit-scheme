#| -*-Scheme-*-

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994,
    1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005,
    2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016,
    2017, 2018, 2019, 2020, 2021, 2022 Massachusetts Institute of
    Technology

This file is part of MIT/GNU Scheme.

MIT/GNU Scheme is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or (at
your option) any later version.

MIT/GNU Scheme is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with MIT/GNU Scheme; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,
USA.

|#

;;;; Debugger Utilities
;;; package: (runtime debugger-utilities)

(declare (usual-integrations))

(define (print-user-friendly-name environment port)
  (let ((name (environment-procedure-name environment)))
    (if name
	(let ((rename (scode-lambda-name->syntax-name name)))
	  (if rename
	      (begin
		(write-string "the special form: " port)
		(write rename port))
	      (begin
		(write-string "the procedure: " port)
		(write name port))))
	(write-string "an unknown procedure" port))))

(define (show-environment-procedure environment port)
  (let ((scode-lambda (environment-lambda environment)))
    (if scode-lambda
	(debugger-presentation port
	  (lambda ()
	    (pretty-print scode-lambda port)))
	(debugger-failure port "No procedure for this environment."))))

(define (write-dbg-name name port)
  (cond ((string? name)
	 (write-string name port))
	((interned-symbol? name)
	 (write-string (symbol->string name) port))
	(else
	 (write name port))))

(define (write-dbg-upcase-name name port)
  (cond ((string? name)
	 (write-string (string-upcase name)))
	((interned-symbol? name)
	 (write-string (string-upcase (symbol->string name)) port))
	(else
	 (write name port))))

(define (debug/read-eval-print-1 environment port)
  (let ((value
	 (debug/eval (prompt-for-expression "Evaluate expression" port)
		     environment)))
    (if (undefined-value? value)
	(debugger-message port "No value")
	(debugger-message port "Value: " value))))

(define (output-to-string length thunk)
  (let ((thunk
	 (lambda ()
	   (call-with-current-continuation
	    (lambda (exit)
	      (bind-condition-handler (list condition-type:error)
		  (lambda (condition)
		    (write-string "<Error while printing: ")
		    (write-condition-report condition (current-output-port))
		    (write-string ">")
		    (exit unspecific))
		thunk))))))
    (let ((x
	   (call-with-truncated-output-string length
	     (lambda (port)
	       (parameterize ((current-output-port port))
		 (thunk))))))
      (if (and (car x) (> length 4))
	  (string-append (string-slice (cdr x) 0 (- length 4))
			 " ...")
	  (cdr x)))))

(define (show-frames environment depth port)
  (debugger-presentation port
    (lambda ()
      (let loop ((environment environment) (depth depth))
	(write-string "----------------------------------------" port)
	(newline port)
	(show-frame environment depth true port)
	(if (environment-has-parent? environment)
	    (begin
	      (newline port)
	      (newline port)
	      (loop (environment-parent environment) (+ depth 1))))))))

(define (show-frame environment depth brief? port)
  (show-environment-name environment port)
  (if (exact-nonnegative-integer? depth)
      (begin
	(write-string "Depth (relative to initial environment): " port)
	(write depth port)
	(newline port)))
  (if (not (and (environment-has-name? environment) brief?))
      (show-environment-bindings environment brief? port)))

(define (no-current-environment port)
  (debugger-failure port "There is no current environment."))

(define (show-environment-name environment port)
  (print-environment-name environment port)
  (newline port))

(define (print-environment-name environment port)
  (write-string "Environment " port)
  (let ((name (environment-name environment)))
    (if name
	(begin
	  (write-string "named: " port)
	  (write name port))
	(begin
	  (write-string "created by " port)
	  (print-user-friendly-name environment port)))))

(define (print-binding binding port)
  (write-string
   (let ((x-size (- (output-port/x-size port) 1)))
     (let ((name
	    (output-to-string (quotient x-size 2)
	      (lambda ()
		(write-dbg-name (car binding) (current-output-port))))))
       (cond ((not (pair? (cdr binding)))
	      (string-append name " is unassigned"))
	     ((macro-reference-trap? (cadr binding))
	      (string-append name " is a syntactic keyword"))
	     (else
	      (let ((s (string-append name " = ")))
		(string-append
		 s
		 (output-to-string (max (- x-size (string-length s)) 0)
				   (lambda ()
				     (write (cadr binding))))))))))
   port)
  (newline port))

(define (debugger-failure port . objects)
  (port/debugger-failure port (message-arguments->string objects)))

(define (debugger-message port . objects)
  (port/debugger-message port (message-arguments->string objects)))

(define (message-arguments->string objects)
  (apply string-append
	 (map (lambda (x) (if (string? x) x (write-to-string x)))
	      objects)))

(define (debugger-presentation port thunk)
  (port/debugger-presentation port thunk))

(define (debugger-pp expression indentation port)
  (parameterize ((param:printer-list-depth-limit debugger:list-depth-limit)
		 (param:printer-list-breadth-limit debugger:list-breadth-limit)
		 (param:printer-string-length-limit
		  debugger:string-length-limit))
    (pretty-print expression port #t indentation)))

(define expression-indentation 4)

(define (show-environment-bindings environment brief? port)
  (let ((bindings
	 (sort (environment-bindings environment)
	   (lambda (a b) (symbol<? (car a) (car b))))))
    (let ((n-bindings (length bindings))
	  (limit
	   (cond ((not brief?) detailed-bindings-limit)
		 ((exact-positive-integer? brief?) brief?)
		 (else brief-bindings-limit)))
	  (finish
	   (lambda (bindings)
	     (newline port)
	     (for-each (lambda (binding)
			 (print-binding binding port))
		       bindings))))
      (cond ((= n-bindings 0)
	     (write-string " has no bindings" port)
	     (newline port))
	    ((<= n-bindings limit)
	     (write-string " has bindings:" port)
	     (newline port)
	     (finish bindings))
	    (else
	     (write-string " has " port)
	     (write n-bindings port)
	     (write-string " bindings (first " port)
	     (write limit port)
	     (write-string " shown):" port)
	     (newline port)
	     (finish (take bindings limit)))))))

(define brief-bindings-limit 16)
(define detailed-bindings-limit 64)

(define (print-reduction-expression rnode port)
  (write-string "Expression (from execution history):" port)
  (newline port)
  (debugger-pp (ctree-reduction-expression rnode)
	       expression-indentation
	       port))

(define (print-reduction-environment rnode port)
  (print-environment (ctree-reduction-environment rnode)
		     port))

(define (print-subproblem-expression snode port)
  (let ((expression (ctree-subproblem-expression snode))
	(subexpression (ctree-subproblem-subexpression snode)))
    (cond ((dbg-expression-undefined? expression)
	   (write-string "Expression unknown" port)
	   (newline port)
	   (write (ctree-subproblem-return-address snode) port))
	  ((dbg-expression-compiled? expression)
	   (write-string "Compiled code expression unknown" port)
	   (newline port)
	   (write (ctree-subproblem-return-address snode) port))
	  ((dbg-printer? expression)
	   (dbg-printer-apply expression #t port))
	  (else
	   (write-string (if (ctree-subproblem-cc-frame? snode)
			     "Compiled code expression (from stack):"
			     "Expression (from stack):")
			 port)
	   (newline port)
	   (if (dbg-expression-undefined? subexpression)
	       (debugger-pp expression expression-indentation port)
	       (begin
		 (debugger-pp
		  (unsyntax-with-substitutions
		   expression
		   (list (cons subexpression subexpression-marker)))
		  expression-indentation
		  port)
		 (newline port)
		 (write-string " subproblem being executed (marked by " port)
		 (write subexpression-marker port)
		 (write-string "):" port)
		 (newline port)
		 (debugger-pp subexpression expression-indentation
			      port)))))))

(define-integrable subexpression-marker '<!>)

(define (print-subproblem-environment snode port)
  (if (ctree-subproblem-has-environment? snode)
      (print-environment (ctree-subproblem-environment snode) port)
      (begin
	(newline port)
	(write-string "There is no current environment." port))))

(define (print-environment environment port)
  (newline port)
  (print-environment-name environment port)
  (if (not (environment-has-name? environment))
      (begin
	(newline port)
	(let ((arguments (environment-arguments environment)))
	  (if (eq? arguments 'unknown)
	      (show-environment-bindings environment #t port)
	      (begin
		(write-string " applied to: " port)
		(write-string
		 (cdr
		  (write-to-string
		   arguments
		   (- (output-port/x-size port) 11)))
		 port)))))))

(define (debug/invoke-restart ctree port)
  (let ((condition (ctree-condition ctree)))
    (let ((restarts
	   (if condition
	       (condition/restarts condition)
	       (bound-restarts))))
      (if (null? restarts)
	  (debugger-failure port "No options to choose from.")
	  (let ((n-restarts (length restarts))
		(write-index
		 (lambda (index port)
		   (write-string (string-pad-left (number->string index) 3)
				 port)
		   (write-string ":" port))))
	    (let ((invoke-option
		   (lambda (n)
		     (invoke-restart-interactively
		      (list-ref restarts (- n-restarts n))
		      condition))))
	      (port/debugger-presentation port
		(lambda ()
		  (if (= n-restarts 1)
		      (begin
			(write-string "There is only one option:" port)
			(write-restarts restarts port write-index)
			(if (prompt-for-confirmation "Use this option" port)
			    (invoke-option 1)))
		      (begin
			(write-string "Choose an option by number:" port)
			(write-restarts restarts port write-index)
			(invoke-option
			 (prompt-for-integer "Option number"
					     1
					     (+ n-restarts 1)
					     port))))))))))))

(define (prompt-for-nonnegative-integer prompt limit port)
  (prompt-for-integer prompt 0 limit port))

(define (prompt-for-integer prompt lower upper port)
  (let loop ()
    (let ((expression
	   (prompt-for-expression
	    (string-append
	     prompt
	     (if lower
		 (if upper
		     (string-append " (" (number->string lower)
				    " through "
				    (number->string (- upper 1))
				    " inclusive)")
		     (string-append " (minimum " (number->string lower) ")"))
		 (if upper
		     (string-append " (maximum "
				    (number->string (- upper 1))
				    ")")
		     "")))
	    port)))
      (cond ((not (exact-integer? expression))
	     (debugger-failure port prompt " must be exact integer.")
	     (loop))
	    ((and lower (< expression lower))
	     (debugger-failure port prompt " too small.")
	     (loop))
	    ((and upper (>= expression upper))
	     (debugger-failure port prompt " too large.")
	     (loop))
	    (else
	     expression)))))

(define (prompt-for-evaluated-value prompt snode port)
  (let ((exp-evaluable? (ctree-subproblem-has-expression? snode)))
    (let ((exp
	   (prompt-for-expression
	    (string-append prompt (if exp-evaluable? " ($ to retry)" ""))
	    port))
	  (env
	   (if (ctree-subproblem-has-environment? snode)
	       (ctree-subproblem-environment snode)
	       (nearest-repl/environment))))
      (if (and exp-evaluable? (eq? exp '$))
	  (debug/scode-eval (ctree-subproblem-expression snode) env)
	  (debug/eval exp env)))))